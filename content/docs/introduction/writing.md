---
title: Writing format
weight: 3
---

# Writing

You can write new posts at the top of the Public page or any Channel page,
or comment on someone else's post.

![writing markdown](/images/guide/writing-markdown.png)

Posts are written with [Markdown formatting](https://commonmark.org/help/). Most scuttlebutt
apps will automatically format and display your writing following markdown styles.
Things like headings, just have a `#` in font of them.

`# hello world`

When you're done, click "Preview & Publish".  This lets you check that your
formatting looks good, then you can actually publish it.

![writing markdown preview](/images/guide/preview.png)

You can mention people using "@" and they will also get a notification on their **Mentions** page.

&nbsp;

**NOTE!**

Posts are permanent!  You can't edit or delete them.  This isn't
an ideological thing, just a technical challenge that we're working on fixing.

So, use your spellcheck, actually look at the Preview, and don't publish in anger.
You won't be able to take it back.

Scuttlebutt works offline just like email clients like Outlook and Thunderbird.
You can write posts and comments offline and they will sync later when you connect again.
Why not take your computer outside and write to your friends from under a tree?

Expect an email-like speed of communication - sometimes people respond quickly,
and sometimes it's more like having a penpal and waiting for the mail to arrive.

### Some places to start

Once you're onboard patchwork, check out the following channels by typing their name
(with #) in the search bar:

* **#new-people:** introduce yourself
* **#faq:** first impressions, what is confusing as a new user?
* **#patchwork:** report bugs, suggestions, etc
* **#scuttlebutt:** technical discussion

Introducing yourself in the #new-people channel is a great way to encounter others you
might like to follow and who might like to follow you. Unless you do so, even though
other people are able to see your posts, it is possible that nobody will notice your
arrival especially if things are busy at the time and this might lead to a lonely and
discouraging experience. We're a friendly bunch so please don't be shy! Come and
tell us a bit about yourself in #new-people so we can help you find other people
that you might be interested in getting to know better.

## Linking To Things

### Profiles and Channels

The simplest form of linking is a reference to a person or a channel. Just start
typing `@` and the person's name and Patchwork will help you out with suggestions
as you type. Hit enter when you have the right one. The same applies to
channels. Just start typing a `#` and the channel's name instead.

You can use multiple channel names in the text of your message, just like hashtags on Twitter.

### Everything else

Linking to an external web page with Markdown is easy, just say

`[display text](https://ssb.nz)`.

[display text](https://ssb.nz)

How do you link to other messages _in_ Scuttlebutt though? Well, first you need
the message id. Right click on a link to the message you want to link to and
choose "Copy Message ID".

![copy msg id](/images/guide/msg-id.png)


Then make a link using the message id as the destination in the brackets. You can put whatever
description you want to display in the square brackets. It will show up just like a linked text.

`[See my last message about dogs here](%SNQIRZ8NU9ujOcr9iXJvlJP0qjdv99J/g4/JMuWYjK0=.sha256)`.

[See my last message about dogs here](%SNQIRZ8NU9ujOcr9iXJvlJP0qjdv99J/g4/JMuWYjK0=.sha256).

Profiles are a little different and normally you just let Patchwork autocomplete
for you when you type `@`.  To do it manually, you can find their "feed id" on their
profile page just under their name.  It's a long random series of characters starting
with an `@`.
